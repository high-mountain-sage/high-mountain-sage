// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const webpack = require('webpack')

module.exports = function (api) {
  api.loadSource(({ addSchemaTypes }) => {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/
    addSchemaTypes(`
      type Pages implements Node @infer {
        categories: [Pages_Categories]
        profiles: [Pages_Profiles]
      }
      type Pages_Categories {
        category: Category
        category_image: Pages_Categories_CategoryImage
      }
      type Pages_Categories_CategoryImage {
        alt_text: String
        photo: String
        placeholder: String
      }
      type Pages_Profiles {
        profile: Profile
      }
      type Product implements Node @infer {
        related_products: [Product_RelatedProducts]
      }
      type Product_RelatedProducts {
        related_product: Product
      }
    `)
  })

  api.onCreateNode((node) => {
    const highQuality = '/upload/'
    const lowQuality = '/upload/q_10/'

    if (node.internal.typeName === 'Category' || node.internal.typeName === 'Product') {
      if (node.public === false) {
        return null
      } else if (node.product_image) {
        node.product_image.placeholder = node.product_image.photo.replace(highQuality, lowQuality)
      }
    }

    if (node.internal.typeName === 'Pages' && node.id === 'home') {
      for (const element in node.categories) {
        const placeholder = node.categories[element].category_image.photo.replace(highQuality, lowQuality)
        node.categories[element].category_image.placeholder = placeholder
      }
    }

    if (node.internal.typeName === 'Testimonial' && node.photo) {
      node.placeholder = node.photo.replace(highQuality, lowQuality)
    }
  })

  api.createPages(async ({ createPage, graphql }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
    const { data } = await graphql(`{
      allProduct {
        edges {
          node {
            id
            slug
            category {
              slug
            }
          }
        }
      }
    }`)

    for (const { node } of data.allProduct.edges) {
      createPage({
        path: `/${node.category.slug}/${node.slug}`,
        component: './src/customTemplates/Product.vue',
        queryVariables: { id: node.id }
      })
    }
  })

  api.chainWebpack((config) => {
    config
      .plugin('moment')
      .use(webpack.IgnorePlugin, [{
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/
      }])
    if (process.env.NODE_ENV === 'production') {
      // config
      //   .plugin('bundle')
      //   .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [{
      //     analyzerMode: 'static',
      //     openAnalyzer: true
      //   }])
      // config
      //   .plugin('stato')
      //   .use(require('@statoscope/webpack-plugin').default)
    }
  })
}
