import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    info: {
      image: '',
      name: ''
    },
    inventory: [],
    menu: false,
    notification: false,
    subTotal: 0,
    type: ''
  },
  getters: {
    getProductInventory (state) {
      return (id) => {
        const productId = state.inventory.find((product) => product.id === id)
        if (productId) {
          return productId.stock
        }
        return -1
      }
    }
  },
  mutations: {
    addPromo (state, item) {
      state.info.name = item.code
      state.notification = true
      state.type = 'PromoNotification'
    },
    addToCart (state, item) {
      state.info.image = item.image
      state.info.name = item.name
      state.notification = true
      state.type = 'AddNotification'
    },
    hideNotification (state) {
      state.info.id = ''
      state.info.image = ''
      state.info.name = ''
      state.notification = false
      state.type = ''
    },
    setInventory (state, inventory) {
      state.inventory = inventory
    },
    toggleMenu (state) {
      state.menu = !state.menu
    },
    updateSubTotal (state, subTotal) {
      state.subTotal = subTotal
    }
  },
  actions: {
    async fetchInventory (context) {
      try {
        const response = await fetch(process.env.GRIDSOME_INVENTORY)
        if (!response.ok) {
          throw new Error('Network response was not OK')
        }
        const data = await response.json()
        context.commit('setInventory', data.inventory)
      } catch (error) {
        console.log(error)
      }
    }
  }
})
