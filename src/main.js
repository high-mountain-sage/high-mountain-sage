// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
// import Vuelidate from 'vuelidate'

import '@fontsource/raleway/latin-400.css'
import rail400 from '@fontsource/raleway/files/raleway-latin-400-normal.woff2'
import '@fontsource/raleway/latin-500.css'
import rail500 from '@fontsource/raleway/files/raleway-latin-500-normal.woff2'
import { config, library } from '@fortawesome/fontawesome-svg-core'
import { faFacebookSquare, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faBars, faCartPlus, faExclamation, faShoppingCart, faSpinner, faTimes, faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import '@fortawesome/fontawesome-svg-core/styles.css'

import DefaultLayout from '~/layouts/Default.vue'
import store from '~/store'

config.autoAddCss = false
library.add(faBars, faCartPlus, faExclamation, faShoppingCart, faSpinner, faTimes, faUser)
library.add(faFacebookSquare, faInstagram)

export default function (Vue, { head, isClient, appOptions }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)

  // FontAwesome
  Vue.component('font-awesome-icon', FontAwesomeIcon)

  appOptions.store = store
  store.dispatch('fetchInventory')

  // Vue-Google Tag Manager
  if (isClient && process.env.NODE_ENV === 'production') {
    const { default: VueGtm } = require('@gtm-support/vue2-gtm')

    Vue.use(VueGtm, {
      id: 'GTM-KH4CKPR',
      defer: false,
      debug: false
    })
  }

  // Vuelidate
  // Vue.use(Vuelidate)

  if (process.env.NODE_ENV === 'production') {
    head.link.push(
      {
        rel: 'preload',
        href: '/assets/fonts/08d1cdf8-4025-4d88-8d13-0c2ddf0e3960.630640cf.woff2',
        as: 'font',
        type: 'font/woff2',
        crossorigin: 'anonymous'
      },
      {
        rel: 'preload',
        href: rail400,
        as: 'font',
        type: 'font/woff2',
        crossorigin: 'anonymous'
      },
      {
        rel: 'preload',
        href: rail500,
        as: 'font',
        type: 'font/woff2',
        crossorigin: 'anonymous'
      }
    )

    head.script.push({
      innerHTML: `
        var MTUserId='0393aa28-d06d-4c79-8859-1265c7a6e2aa';var MTFontIds = new Array();MTFontIds.push("717832");
        (function() {
          var mtTracking = document.createElement('script');mtTracking.type='text/javascript';mtTracking.async='true';
          mtTracking.src='https://highmountainsage.com/mtiFontTrackingCode.js';(document.getElementsByTagName('body')[0]).appendChild(mtTracking);
        })();
      `,
      body: true
    })
  }

  head.script.push({
    async: true,
    body: true,
    src: 'https://cdn.snipcart.com/themes/v3.0.31/default/snipcart.js'
  })
}
