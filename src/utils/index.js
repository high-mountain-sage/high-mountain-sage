const nestedPath = (obj) => {
  return `/${obj.category.slug}/${obj.slug}/`
}

export {
  nestedPath
}
