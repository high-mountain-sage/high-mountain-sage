module.exports = {
  purge: {
    content: [
      './src/**/*.html',
      './src/**/*.vue'
    ],
    enabled: true,
    mode: 'all',
    options: {
      safelist: [
        /.-enter/, /.-enter-active/, /.-enter-to/,
        /.-leave/, /.-leave-active/, /.-leave-to/,
        'h-12', 'svg-inline--fa', 'w-auto'
      ] // used in snipcart-templates.html
    }
  },
  theme: {
    extend: {
      colors: {
        gray: {
          50: '#fbfaf9',
          100: '#faf9f7',
          200: '#e8e6e1',
          300: '#d3cec4',
          400: '#b8b2a7',
          500: '#a39e93',
          600: '#625d52',
          700: '#504a40',
          800: '#423d33',
          900: '#27241d'
        },
        red: {
          100: '#ffeeee',
          200: '#facdcd',
          300: '#f29b9b',
          400: '#e66a6a',
          500: '#d64545',
          600: '#a61b1b',
          700: '#911111',
          800: '#780a0a',
          900: '#610404'
        },
        teal: {
          100: '#e6fffa',
          200: '#b2f5ea',
          300: '#81e6d9',
          400: '#4fd1c5',
          500: '#38b2ac',
          600: '#319795',
          700: '#2c7a7b',
          800: '#285e61',
          900: '#234e52'
        },
        yellow: {
          100: '#fffaee',
          200: '#fffaeb',
          300: '#fcefc7',
          400: '#f8e3a3',
          500: '#e9b949',
          600: '#c99a2e',
          700: '#a27c1a',
          800: '#7c5e10',
          900: '#513c06'
        }
      },
      height: {
        72: '18rem',
        88: '22rem',
        96: '24rem'
      },
      inset: {
        '-1': '-0.25rem'
      },
      maxWidth: {
        '2xs': '16rem'
      },
      minWidth: {
        sm: '24rem'
      },
      padding: {
        0.5: '0.125rem'
      }
    },
    fontFamily: {
      body: 'Raleway, sans-serif',
      display: ['Modern Twenty W00', 'serif']
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px'
    }
  },
  variants: {
    extend: {
      margin: ['first', 'last']
    }
  },
  plugins: [
    function ({ addBase, addUtilities, config }) {
      addBase({
        body: {
          'background-color': '#faf9f7',
          'background-image': 'url("../../src/assets/img/topography_brown.min.svg")',
          'font-family': config('theme.fontFamily.body'),
          'font-variant-numeric': 'lining-nums'
        }
      })
      addUtilities({
        '.gradient-bottom': {
          'background-image': 'linear-gradient(to bottom, transparent 33%, #faf9f7)',
          height: '100%',
          left: '0',
          position: 'absolute',
          top: '0',
          width: '100%'
        },
        '.gradient-right': {
          'background-image': 'linear-gradient(to right, transparent 33%, #faf9f7)',
          height: '100%',
          left: '0',
          position: 'absolute',
          top: '0',
          width: '100%'
        },
        '.hero': {
          'background-color': '#faf9f7',
          'background-image': 'url("../../src/assets/img/topography_brown.min.svg")'
        }
      }, ['responsive'])
    }
  ]
}
