import { mount } from '@vue/test-utils'

import AddNotification from '@/components/AddNotification.vue'

describe('AddNotification.vue', () => {
  let propsData

  beforeEach(() => {
    propsData = {
      info: {
        image: 'static/images/tree_mint__oil.jpg',
        name: 'Tree Mint Oil'
      }
    }
  })

  it('displays an "added to cart" message with the product name', () => {
    const wrapper = mount(AddNotification, { propsData })
    const message = wrapper.find('p')

    expect(message.text()).toMatch(wrapper.props().info.name)
    expect(message.text()).toMatch('added to cart')
  })

  it('displays an image of the product', () => {
    const wrapper = mount(AddNotification, { propsData })
    const image = wrapper.find('img')

    expect(image.attributes().src).toBe(wrapper.props().info.image)
  })

  it('adjusts font size based on the length of product\'s name', async () => {
    const wrapper = mount(AddNotification, { propsData })

    await wrapper.setProps({
      info: {
        name: 'Tree Mint Legendary Kit'
      }
    })

    const productTitle = wrapper.find('span')
    expect(productTitle.classes('text-lg')).toBe(true)
  })
})
