import { mount } from '@vue/test-utils'

import PromoNotification from '@/components/PromoNotification.vue'

describe('PromoNotification.spec.js', () => {
  it('displays an "applied to cart" message with the promo name', () => {
    const wrapper = mount(PromoNotification, {
      propsData: {
        info: {
          name: 'spec'
        }
      }
    })
    const message = wrapper.find('p')

    expect(message.text()).toMatch(wrapper.props().info.name)
    expect(message.text()).toMatch('applied to cart')
  })
})
