import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'

import ShippingGauge from '@/components/ShippingGauge'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('ShippingGauge.vue', () => {
  let state
  let store

  beforeEach(() => {
    state = {
      subTotal: 0
    }

    store = new Vuex.Store({
      state
    })
  })

  it('shows how close a customer is to unlocking free shipping', async () => {
    const wrapper = mount(ShippingGauge, {
      data () {
        return {
          freeShippingLimit: 65
        }
      },
      store,
      localVue
    })

    const gauge = wrapper.find('div>span')
    const message = wrapper.find('p')

    console.log(wrapper.freeShippingLimit)

    // Cart is empty
    expect(gauge.attributes('style')).toBe('width: 0%;')
    expect(message.text()).toBe('Free Shipping, $65.00 away.')

    // Cart contains $32.50 of items
    store.state.subTotal = 32.5
    await localVue.nextTick()

    expect(gauge.attributes('style')).toBe('width: 50%;')
    expect(message.text()).toBe('Free Shipping, $32.50 away.')

    // Cart contains $65.00 of items
    store.state.subTotal = 65
    await localVue.nextTick()

    expect(gauge.attributes('style')).toBe('width: 100%;')
    expect(message.text()).toBe('Free Shipping unlocked!')

    // Cart contain $100 of items
    store.state.subTotal = 100
    await localVue.nextTick()

    expect(gauge.attributes('style')).toBe('width: 100%;')
    expect(message.text()).toBe('Free Shipping unlocked!')
  })
})
