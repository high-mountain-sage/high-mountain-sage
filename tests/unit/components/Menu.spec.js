import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'

import Menu from '@/components/Menu.vue'
import MenuLink from '@/components/MenuLink.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Menu.vue', () => {
  let mutations
  let state
  let store

  beforeEach(() => {
    state = {
      menu: true
    }

    mutations = {
      toggleMenu: jest.fn()
    }

    store = new Vuex.Store({
      state,
      mutations
    })
  })

  it('appears when expected', () => {
    const wrapper = shallowMount(Menu, {
      store,
      localVue
    })
    const menu = wrapper.find('div')
    expect(menu.exists()).toBe(true)
  })

  it('closes when a menu link is clicked', async () => {
    const wrapper = mount(Menu, {
      store,
      localVue
    })
    const menuItem = wrapper.findComponent(MenuLink)
    const menuLink = menuItem.find('a')
    await menuLink.trigger('click')
    expect(mutations.toggleMenu).toHaveBeenCalled()

    store.state.menu = false
    await localVue.nextTick()

    const menu = wrapper.find('div')
    expect(menu.exists()).toBe(false)
  })
})
