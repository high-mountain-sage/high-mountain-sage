import { mount } from '@vue/test-utils'

import CompanyLogo from '@/components/CompanyLogo.vue'

describe('CompanyLogo', () => {
  it('renders correctly', () => {
    const wrapper = mount(CompanyLogo)

    expect(wrapper).toMatchSnapshot()
  })
})