import { shallowMount } from '@vue/test-utils'

import ProductBuy from '@/components/ProductBuy.vue'

describe('ProductBuy.vue', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ProductBuy, {
      propsData: {
        product: {
          id: 'uuid',
          name: 'Test Product',
          category: {
            slug: 'test-category'
          },
          product_image: {
            photo: 'https://photo.cdn'
          },
          price: 39.95,
          slug: 'test-product',
          weight: 50
        }
      },
      stubs: ['font-awesome-icon']
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the class "snipcart-add-item"', () => {
    expect(wrapper.classes()).toContain('snipcart-add-item')
  })

  it('contains the required attributes for snipcart and easypost', () => {
    expect(wrapper.attributes('data-item-id')).toEqual(wrapper.props().product.id)
    expect(wrapper.attributes('data-item-name')).toEqual(wrapper.props().product.name)
    expect(wrapper.attributes('data-item-price')).toEqual(String(wrapper.props().product.price))
    expect(wrapper.attributes('data-item-url')).toEqual(wrapper.vm.path)
    expect(wrapper.attributes('data-item-weight')).toEqual(String(wrapper.props().product.weight))
  })
})
