import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'

import { config, library } from '@fortawesome/fontawesome-svg-core'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import CartNotification from '@/components/CartNotification.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
config.autoAddCss = false
library.add(faTimes)
localVue.component('font-awesome-icon', FontAwesomeIcon)

describe('CartNotification.vue', () => {
  let mutations
  let state
  let store

  beforeEach(() => {
    mutations = {
      hideNotification: jest.fn()
    }

    state = {
      notification: true,
      info: {
        name: 'spec'
      }
    }

    store = new Vuex.Store({
      state,
      mutations
    })
  })

  it('appears when expected', () => {
    const wrapper = shallowMount(CartNotification, {
      store,
      localVue
    })
    const cartNotification = wrapper.find('div')
    expect(cartNotification.exists()).toBe(true)
  })

  it('closes when expected', async () => {
    const wrapper = mount(CartNotification, {
      localVue,
      store
    })
    const close = wrapper.findComponent(FontAwesomeIcon)
    await close.trigger('click')
    expect(mutations.hideNotification).toHaveBeenCalled()

    store.state.notification = false
    await localVue.nextTick()

    const cartNotification = wrapper.find('div')
    expect(cartNotification.exists()).toBe(false)
  })
})
