import { mount } from '@vue/test-utils'

import StructuredBlog from '@/components/StructuredBlog'

describe('StructuredBlog.vue', () => {
  it('renders correctly', () => {
    const wrapper = mount(StructuredBlog, {
      propsData: {
        blog: {
          publish_iso: '2021-02-27',
          title: 'The 7 Deadly Sins of Beard Care'
        }
      }
    })

    expect(wrapper.html()).toBe(
`<div>
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "BlogPosting",
      "headline": "The 7 Deadly Sins of Beard Care",
      "datePublished": "2021-02-27"
    }
  </script>
</div>`
    )
  })
})
