import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCartPlus, faExclamation, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import ProductBuy from '@/components/ProductBuy.vue'
import ProductCard from '@/components/ProductCard.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
library.add(faCartPlus, faExclamation, faTimes)
localVue.component('font-awesome-icon', FontAwesomeIcon)

describe('ProductCard.vue', () => {
  let state
  let getters
  let store

  beforeEach(() => {
    state = {
      inventory: 1
    }
    getters = {
      getProductInventory: (state) => () => state.inventory
    }

    store = new Vuex.Store({
      state,
      getters
    })
  })

  it('changes button/icon depending on inventory status', async () => {
    const wrapper = mount(ProductCard, {
      propsData: {
        product: {
          id: 'uuid',
          name: 'Test Product',
          category: {
            slug: 'generic-products'
          },
          price: 39.95,
          product_image: {
            alt_text: 'a generic product',
            photo: 'https://photo.cdn/test-product',
            placeholder: 'https://photo.cdn/q-10/test-product'
          },
          scent_profile: {
            name: 'classic'
          },
          scent_notes: {
            notes: ['sugar', 'spice', 'everything nice']
          },
          size: 'uuid-size',
          slug: 'test-product',
          status: {
            id: '5484176a-e7b9-4b08-a293-ddd5860a6d78'
          }
        }
      },
      store,
      stubs: ['g-link'],
      localVue
    })

    // In Stock - Snipcart Inventory
    expect(wrapper.findComponent(ProductBuy).exists()).toBe(true)

    // Out of Stock - Snipcart Inventory
    store.state.inventory = 0
    await localVue.nextTick()

    expect(wrapper.find('div[title="Out of Stock"]').exists()).toBe(true)

    // Status Unknown - Snipcart Inventory
    store.state.inventory = -1
    await localVue.nextTick()

    // Defaults to status in content file
    expect(wrapper.findComponent(ProductBuy).exists()).toBe(true)
  })
})
