import { createLocalVue, mount } from '@vue/test-utils'

import { config, library } from '@fortawesome/fontawesome-svg-core'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import flushPromises from 'flush-promises'
import Vuelidate from 'vuelidate'

import MailingList from '@/components/MailingList.vue'

jest.mock('fetch')

const localVue = createLocalVue()

config.autoAddCss = false
library.add(faSpinner)
localVue.component('font-awesome-icon', FontAwesomeIcon)
localVue.use(Vuelidate)

describe('MailingList.vue', () => {
  beforeEach(() => {
    fetch.mockClear()
  })

  it('submits an email address for verification', async () => {
    const wrapper = mount(MailingList, { localVue })
    fetch.post.mockResolvedValue({ data: {} })

    await wrapper.setData({ mailingData: { email: 'example@gmail.com' } })

    const form = wrapper.find('form')
    form.trigger('submit.prevent')

    await flushPromises()
    expect(fetch.post).toHaveBeenCalledWith(wrapper.vm.url, JSON.stringify(wrapper.vm.mailingData))
  })

  it('renders a "thank you" message when verification succeeds', async () => {
    const wrapper = mount(MailingList, { localVue })
    fetch.post.mockResolvedValue({ data: {} })

    await wrapper.setData({ mailingData: { email: 'example@gmail.com' } })

    const form = wrapper.find('form')
    form.trigger('submit.prevent')

    await flushPromises()
    expect(wrapper.vm.verified).toBe(true)
    expect(wrapper.text()).toMatch('Thank You!')
  })

  it('renders an error message when verification fails', async () => {
    const wrapper = mount(MailingList, { localVue })
    fetch.post.mockRejectedValue({
      response: {
        data: {
          error: 'Please enter a valid e-mail address'
        }
      }
    })

    await wrapper.setData({ mailingData: { email: 'example@gmail.com' } })

    const form = wrapper.find('form')
    form.trigger('submit.prevent')

    await flushPromises()
    expect(wrapper.text()).toMatch('Please enter a valid e-mail address')
  })
})
