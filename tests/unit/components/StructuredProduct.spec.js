import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'

import StructuredProduct from '@/components/StructuredProduct'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('StructuredProduct.vue', () => {
  let state
  let getters
  let store

  beforeEach(() => {
    state = {
      inventory: 1
    }
    getters = {
      getProductInventory: (state) => () => state.inventory
    }

    store = new Vuex.Store({
      state,
      getters
    })
  })

  it('renders correctly', () => {
    const wrapper = mount(StructuredProduct, {
      propsData: {
        product: {
          name: 'Original Soap',
          category: {
            slug: 'soaps'
          },
          content: `<p>Made in collaboration with our friends at <a href="https://www.wildwaterssoapery.com" class="font-medium text-teal-700 hover:text-teal-800" target="_blank" rel="nofollow noopener noreferrer">Wild Waters Soapery</a></p>
          <br>
          <p>The scent that started it all and for which our company is named after. An all natural shampoo bar to cleanse your "beer'd" all while smelling like the mountainous woods of Northern Utah. Original Beer'd Soap welcomes you into the shower with warm, subtle, notes of cedarwood, sage, and orange. Gentle and cleansing for both facial and head hair.</p>
          <br>
          <p>Buy three or more soaps and receive a bulk discount of $4 off your order.</p>`,
          product_image: {
            photo: 'https://cdn.com/original-soap.jpg'
          },
          price: 8,
          sku: 'SP-CSC-OG',
          slug: 'original-soap',
          status: {
            id: '5484176a-e7b9-4b08-a293-ddd5860a6d78'
          }
        }
      },
      store,
      localVue
    })

    expect(wrapper.html()).toBe(
`<div>
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Product",
      "name": "Original Soap",
      "brand": {
        "@type": "Brand",
        "name": "High Mountain Sage"
      },
      "description": "Made in collaboration with our friends at Wild Waters Soapery The scent that started it all and for which our company is named after. An all natural shampoo bar to cleanse your \\"beer'd\\" all while smelling like the mountainous woods of Northern Utah. Original Beer'd Soap welcomes you into the shower with warm, subtle, notes of cedarwood, sage, and orange. Gentle and cleansing for both facial and head hair. Buy three or more soaps and receive a bulk discount of $4 off your order.",
      "image": "https://cdn.com/original-soap.jpg",
      "mpn": "HMS-SP-CSC-OG",
      "offers": {
        "@type": "Offer",
        "price": 8,
        "priceCurrency": "USD",
        "url": "https://highmountainsage.com/soaps/original-soap/"
      },
      "sku": "SP-CSC-OG"
    }
  </script>
</div>`
    )
  })
})
