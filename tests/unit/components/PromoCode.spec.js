import { mount } from '@vue/test-utils'

import PromoCode from '@/components/PromoCode.vue'

describe('PromoCode', () => {
  Object.defineProperty(window, 'Snipcart', {
    writable: true,
    value: {
      api: {
        cart: {
          applyDiscount: jest.fn((code) => true)
        }
      }
    }
  })

  it('applies the promo code when clicked', async () => {
    const wrapper = mount(PromoCode, {
      propsData: {
        code: 'SPEC'
      }
    })

    await wrapper.trigger('click')
    expect(window.Snipcart.api.cart.applyDiscount).toHaveBeenCalledWith(wrapper.props().code)
  })
})
