// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  prefetch: { mask: '^$' },
  plugins: [
    {
      use: '@gridsome/plugin-critical'
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        exclude: ['/contact/thank-you/', '/link-in-bio/', '/privacy/', '/terms/']
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/blog/*.md',
        refs: {
          author: 'Profile'
        },
        typeName: 'Blog'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/categories/*.md',
        refs: {
          testimonial: 'Testimonial'
        },
        typeName: 'Category'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/faqs/*.md',
        typeName: 'FAQ'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/pages/*.md',
        typeName: 'Pages'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/products/*.md',
        refs: {
          category: 'Category',
          scent_profile: 'ScentProfile',
          scent_notes: 'ScentNotes',
          series: 'Series',
          size: 'Size',
          status: 'Status'
        },
        typeName: 'Product'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/profiles/*.md',
        typeName: 'Profile'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/scent_profiles/*.md',
        typeName: 'ScentProfile'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/scent_notes/*.md',
        typeName: 'ScentNotes'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/series/*.md',
        typeName: 'Series'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/sizes/*.md',
        typeName: 'Size'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/status/*.md',
        typeName: 'Status'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/tips_tricks/*.md',
        typeName: 'TipsTricks'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: './content/testimonials/*.md',
        typeName: 'Testimonial'
      }
    },
    {
      use: 'gridsome-plugin-tailwindcss'
    }
  ],
  siteName: 'High Mountain Sage',
  siteUrl: 'https://highmountainsage.com/',
  templates: {
    Blog: '/blog/:slug',
    Category: '/:slug',
    TipsTricks: '/tips-and-tricks/:slug'
  },
  transformers: {
    remark: {
      autolinkHeadings: false,
      plugins: [
        'remark-attr'
      ],
      slug: false
    }
  }
}
