---
id: 6b0043c8-c37c-4f43-8a4e-b7778dfb7ba2
name: Soaps
order: 3
shortName: Soaps
slug: soaps
public: true
testimonial:
---
Made with beer brewed in house and then completed by our friends at Wild Waters Soapery. Our Beer'd Soap cleanses your face fuzz, all while providing the necessary nutrients and conditioning your beard can't go without. Made with natural ingredients, no added dyes or unnecessary chemicals here.
