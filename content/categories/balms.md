---
id: bd70bd4f-0b5b-42da-bc6d-79d2bdd31d9c
name: Balms
order: 1
shortName: Balms
slug: balms
public: true
testimonial: 
---
Our balms are great for moisturizing your beard and skin, while also providing a medium to light styling hold. Our recipe is more like a beard butter than other balms.
