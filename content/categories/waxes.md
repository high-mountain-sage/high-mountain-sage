---
id: 4d5ea960-fc7f-4909-b203-fe69e7a66cac
name: Mustache Waxes
order: 2
shortName: Waxes
slug: mustache-waxes
public: true
testimonial: 54969280-513f-494b-88a2-981ba779ab74
---
Stiffer than balm to provide greater styling hold for your mustache. Small enough to stash in your pockets.
