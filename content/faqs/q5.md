---
name: q5
question: Other Than Facial Hair, Can Your Products Be Used In Different Ways?
---
* **Balms**, can style your head hair as much as the hair of your beard. They also make excellent skin conditioners for cracked feet, dry hands, etc.
* **Beard Oils**, can be used in hair as a leave-in softener and conditioner. Perhaps more so than balms, beard oils can also be applied to the skin, as a highly nutritious massage oils, in addition to cracked feet, dry hands, etc.
* **Mustache Waxes**, can be used to help style parts of the beard that are more difficult to manage, allow for more creative/outlandish styles for beard and hair. However waxes shouldn't be used solely as a moisturizing or conditioning agent.