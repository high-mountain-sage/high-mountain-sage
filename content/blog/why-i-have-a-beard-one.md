---
author: 1c1f8066-e552-4adc-b5e5-c9da5196752f
title: >-
    Why I have a beard - Part One: Confidence
slug: why-i-have-a-beard-one
excerpt: >-
    Look around you. American culture, and seemily society at large, is obessed with beards. Business men can be seen with short, sharp lined beards. Famous athletes are sporting big and untamed beards. Even the average, everyday man can be seen with some sort of facial hair, be it a trimmed moustache or the purposeful 5 o'clock shadow. I admit my reason for growing my beard out longer than it had ever been before was influenced, in part, by all the "beard hype" back in the summer of...
publish_date: 2019-10-23
updated_date: 2019-10-23
---
Look around you. American culture, and seemingly society at large, is obessed with beards. Businessmen can be seen with short, sharp lined beards. Famous athletes are sporting big and untamed beards. Even the average, everyday man can be seen with some sort of facial hair, be it a trimmed moustache or the purposeful 5 o'clock shadow. I admit my reason for growing my beard out longer than it had ever been before was influenced, in part, by all the "beard hype" back in the summer of 2016. However, I have had a beard and/or facial hair, except for a few short stints when I wasn't allowed, since my peach fuzz started to turn into the real deal well over a decade ago. My reasons for growing a beard and having facial hair are very personal, having to do with my feeling more confident as a person, my own form of rebellion, as well as inspiring a greater desire for improved self-care.

<br>

This first article of a three part series explores the confidence building factor as one of the reasons for why I grew and maintain a beard. Also included is a simple tip on how you can improve your own confidence. It is with great hope and desire that this series inspires you to explore more in depth your personal reasons for growing, or not growing, a beard and/or facial hair, as well as to help you improve your sense of confidence in all areas of your life.

![A teenage Bryan with sparse wispy facial hair](https://res.cloudinary.com/high-mountain-sage/image/upload/v1579804888/teen_bryan_ptlt8k.jpg){.mb-2 .mt-6}

The year is 2006, I'm a senior in high school and my sideburns are coming in "fuller" than ever. On my chin I have the ability to grow a wispy goatee (see photo above). By today's standards people would probably think I looked odd, scraggly, and underwhelming, perhaps even greasy and gross. At the time though I felt awesome. The ability to grow more facial hair than most boys my age felt good. I only had to trim once every two weeks. Fast forward a year later, 2007, my first year in college, and I was able to connect the sideburns with the goatee. Still wispy, still underwhelming, but I felt super confident with it.

<br>

In my day job as a mental health therapist I work to help others enhance their ability to recognize and increase the consciousness of their inner power. Much of the time, a person's source of inner anguish can be tied to their false thoughts and erroneous feelings about a supposed poor physical apperance. Those thoughts and feelings then lead to actions or behaviors that can increase, amplify, and make worse their already negative view of themselves, inwardly and outwardly, leading to overeating, not showering, not exercising, not shaving, trimming or keeping their facial hair tidy. One of the best ways to improve and obtain confidence is by maintaining a regular routine, and a higher standard in personal grooming.

## Pro Tip
{.font-display .mb-1 .mt-6 .text-2xl}

Speaking from firsthand experience, there is a difference in my mental, emotional, and physical well-being when I take the extra effort to tidy up my beard with a trim, apply mustache wax or balm for style. So, here is the pro tip...

<br>

If you want to have more confidence, put into practice a mindful grooming routine. I emphasize *practice*.

<br>

Just like learning to play an instrument, or any kind of skill, confidence must be practiced, and put into action. Set aside specific practice time each day for five minutes to try styling your hair a different way, run a comb through your beard after applying some beard oil, or tidy up that neckline with a quick shave. Confidence is not something someone is born with, but rather built through simple actions and concentrated efforts on a daily basis.

<br>

Taking risks and doing something that feels uncomfortable is what makes a person feel confident, even though that sounds and feels contradictory. That is why I grew a beard. That is why I take time and energy to maintain, groom, and show off my beard. Sometimes I experience those negative thoughts and poor self-image, just like most everyone out there. However, with growing a beard and working to maintain and practice an improved grooming routine, my confidence has absolutely improved. So will yours.