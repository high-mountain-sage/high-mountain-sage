---
author: 1c1f8066-e552-4adc-b5e5-c9da5196752f
title: Don't Be Fooled
slug: dont-be-fooled
excerpt: >-
    Some grooming companies like to advertise and sell bold and very long lasting scents. There is nothing wrong with that, unless of course that means you are being exposed to unhealthy and unsafe ingredients. Don't be fooled, many long lasting...
publish_date: 2021-04-01
updated_date: 2021-04-01
---
Some grooming companies like to advertise and sell bold and very long lasting scents. There is nothing wrong with that, unless of course that means you are being exposed to unhealthy and unsafe ingredients. Don't be fooled, many long lasting and strongly scented products could mean you are risking your health and safety.

<br>

[IFRA](https://ifrafragrance.org/safe-use/introduction){.font-medium .text-teal-700 .hover:text-teal-800}, the International Fragrance Association, has specific rules “based on scientific evidence and consumer insights,” that are set to help protect you, the consumer, from harsh chemicals, cancer causing agents, and other nasty stuff. Their rules are recognized around the world. In case you are wondering, High Mountain Sage ALWAYS follows the safety rules established by IFRA because YOUR safety is paramount.

<br>

I won’t call any particular company out here... However, from personal purchasing experience, I know there are many beard and grooming companies that just whip products together willy nilly, that are nice smelling at first, but then cause headaches and nausea later on. These other companies don’t take into account or even understand that the ingredients or fragrances they are using have to be used safely.

<br>

When you purchase and use [High Mountain Sage](/products/){.font-medium .text-teal-700 .hover:text-teal-800} products you can rest assured that you are safe. Our scents are purposefully subtle, not just because we believe less is more, but because we care about you.