---
author: 1c1f8066-e552-4adc-b5e5-c9da5196752f
title: >-
    A Letter To Our Customers: COVID-19
slug: letter-to-our-customers-covid-19
excerpt: >-
    We've all seen the news and read the headlines about COVID-19 and how it is spreading. It has not been easy to stay calm and weather out this storm with poise, yet I am doing my best, and I hope to support all of you as you endeavour to do the same. With a brand new baby at home..
publish_date: 2020-04-01
updated_date: 2020-04-01
---
Family and Friends of High Mountain Sage,

<br>

We've all seen the news and read the headlines about COVID-19 and how it is spreading. It has not been easy to stay calm and weather out this storm with poise, yet I am doing my best, and I hope to support all of you as you endeavour to do the same. With a brand new baby at home I am indeed being careful to stay away from possible infection and will do everything I can to promote and support your health, which has always been a part of our [mission](/about/){.font-medium .text-teal-700 .hover:text-teal-800} at High Mountain Sage - Beard and Hair CO.

<br>

As of now we remain open for business with our online store, which is fully operational with beard oils, balms, and mustache waxes available for purchase. I promise that we are doing what we can to help slow the spread of COVID-19 and to keep every one of you, our loyal customers and your families, safe. As such every order is being prepared for shipping or delivery only after our hands are thoroughly washed and sanitized, followed by donning nitrile gloves and face masks.

<br>

Our economy depends on your continued support of small businesses, like High Mountain Sage - Beard & Hair CO. Please stay safe, follow the guidelines of the CDC and local governments, and do you best to stay calm through all of this and what may still come.

<br>

Sincerely,

