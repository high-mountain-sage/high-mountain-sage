---
id: 0b55e54b-9b78-4626-b675-845369ee2505
name: Jono Huppi
title: CTO
profile_image:
    alt_text: >-
        A portrait of Jono Huppi, the Chief Technology Officer at High Mountain Sage
    photo: >-
        https://res.cloudinary.com/high-mountain-sage/image/upload/v1582219041/jono_o39xj0.jpg
---