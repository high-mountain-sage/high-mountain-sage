---
id: home
tag_line: Be Subtle
categories:
  - category: 45fb992b-aac4-4411-821b-e80f3f32decd
    category_image:
      alt_text: A bottle of High Mountain Sage beard oil.
      photo:
        https://res.cloudinary.com/high-mountain-sage/image/upload/v1596749289/smoking_jacket__multi_zpis6k.jpg
  - category: bd70bd4f-0b5b-42da-bc6d-79d2bdd31d9c
    category_image:
      alt_text: A jar of High Mountain Sage balm.
      photo:
        https://res.cloudinary.com/high-mountain-sage/image/upload/v1616623194/original_balm__single_dyuf3p.jpg
  - category: 4d5ea960-fc7f-4909-b203-fe69e7a66cac
    category_image:
      alt_text: A tin of High Mountain Sage mustache wax.
      photo:
        https://res.cloudinary.com/high-mountain-sage/image/upload/v1616623192/tree_mint_wax__single_qpz6rh.jpg
---