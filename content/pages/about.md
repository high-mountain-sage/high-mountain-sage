---
id: about
mission: High Mountain Sage - Beard & Hair Co aims to provide subtle luxury in the form of handcrafted beard, hair, and skin products that promote and accentuate the unique style, health, and charm of men, both locally and at large.
profiles:
    - profile: 1c1f8066-e552-4adc-b5e5-c9da5196752f
    - profile: 0b55e54b-9b78-4626-b675-845369ee2505
---
High Mountain Sage - Beard & Hair CO is a start-up company, who formed in October of 2017, and debuted their Original scented beard oil as their first of many luxury beard care products. They continue to grow their product lines to meet the needs of facial hair enthusiasts, both locally and at large.
<br><br>
Our Founder and CEO, Bryan Rieben, has been a long time beard aficionado, growing what facial hair he could in his youth. He went from short sideburns and a sparse goatee, to having a beard for nearly a decade. It wasn't until 2016 that Bryan was first introduced to beard oils and balms, he then chose to grow out his beard longer than he ever had (despite the disapproval of his wife and mother).
<br><br>
After experimenting and using "big brand" beard products, Bryan decided he could handcraft beard oils for his own personal use that could still be high quality. So, in early to mid 2017 Bryan's beard oil making journey began. Others caught wind of his beard oils, and by working with a friend from his youth, Jono Huppi, they started producing beard oils for friends and family on a larger scale. What began as a hobby very quickly grew into High Mountain Sage - Beard and Hair CO.
