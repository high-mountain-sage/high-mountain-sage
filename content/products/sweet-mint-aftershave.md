---
id: 9b2afccc-a43c-44b2-975e-d1920477d2a7
category: 5b59c55e-1aa2-40a4-8620-9bda32cc9bfc
name: Sweet Mint Aftershave
slug: sweet-mint-aftershave
product_image:
  alt_text: >-
    Bottles of Sweet Mint aftershave surrounded by vanilla beans, peppermint leaves, and dried cloves.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1600292999/sweet_mint_aftershave_thq7ip.jpg
ingredients:
  - Silver Rum (41% alc)
  - Witch Hazel
  - Vegetable Glycerin
  - Grapeseed Oil infused with Vanilla
  - Mint Extract
  - Tea Tree, Pepper Mint, and Clove Essential Oils
  - Isopropyl
item_number: 1
price: 25
scent_profile: 820b4505-27ba-4c2e-beb8-51841b82a38a
scent_notes: 22eaa9f9-a758-4e56-a0bf-4d0ba15ab89b
series: a482e706-3075-40b1-af09-87009ae13603
size: 7acd5143-d0b7-4e74-9775-58bbfa9bfbe6
sku: AF-CSC-SM
weight: 123
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: a3f74318-238d-4604-9a29-3213022e9b52 # Tree Mint Oil
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
  - related_product: 944cc97d-91f5-41a9-a094-0767f928f0dc # Tree Mint Mustache Wax
---
Sweet Mint aftershave is crisp and invigorating, and provides a smooth cooling effect on freshly shaved skin. The scent is unique, manly, and satisfying, with subtle notes of mint medley, tea tree, clove and vanilla. It is finished off with handcrafted Silver Rum, a spirit distilled locally in Utah, and witch hazel that helps round out the aroma while providing the cleansing/healing properties you expect from quality shaving products. Your shaving experience just got upgraded.

<br>

As with all High Mountain Sage products, we handcraft Sweet Mint Aftershave in small batches and with as many natural ingredients as possible. These ingredients will separate. Shake well before each use. To maximize shelf life and effectiveness, store in a cool, dry location that is free of direct sunlight.
