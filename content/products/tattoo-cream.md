---
id: 393c9f54-ed6e-45fa-8423-8d9814c60a7c
category: bd70bd4f-0b5b-42da-bc6d-79d2bdd31d9c
name: Tattoo Cream
slug: tattoo-cream
product_image:
  alt_text: >-
    A cardboard jar of Tattoo Cream against a clean background.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1638380128/tattoo_cream__single_k0oz4s.jpg
ingredients:
  - Hemp Butter
  - Shea Butter
  - Mango Butter*
  - Babasuu Oil*
  - Hemp Oil*
  - Meadowfoam Seed Oil*
  - Carnauba Wax*
  - Beeswax
  - Raspberry Seed Oil*
  - Rose Hip Oil*
  - Helichrysum Oil*
  - Myrrh Essential Oil*
item_number: 1
price: 30
scent_profile: 61c5d79c-b016-494d-b8b7-2ec0a4f550fe
scent_notes: bc0c7d31-8ec6-4855-9568-7a3eb0ad54ea
series: a482e706-3075-40b1-af09-87009ae13603
size: 7acd5143-d0b7-4e74-9775-58bbfa9bfbe6
sku: BM-CSC-TC
weight: 75
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
  - related_product: a3f74318-238d-4604-9a29-3213022e9b52 # Tree Mint Oil
---
This non-greasy tattoo cream will keep your old tattoos looking fresher and brighter by making the colors "pop". We chose every natural ingredient (including 9 certified organic ingredients) for a specific purpose, ensuring your body art is properly cared for over the years.

<br>

The light nutty and earthy scent is a natural byproduct of all the ingredients blending together. Nothing synthetic or unnecessary has been added. All killer, no filler.

<br>

As an added bonus we've housed this stellar product in earth friendly biodegradable packaging. Talk about sustainability!

<br>
<br>

Ingredients followed by an asteriks (*) are certified organic