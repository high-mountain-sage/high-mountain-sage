---
id: a3f74318-238d-4604-9a29-3213022e9b52
category: 45fb992b-aac4-4411-821b-e80f3f32decd
name: Tree Mint Oil
slug: tree-mint-oil
product_image:
  alt_text: >-
    A bottle of Tree Mint beard oil surrounded by vanilla beans and peppermint
    leaves.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1584637211/tree_mint_oil__single_txrtnu.jpg
ingredients:
  - Castor Oil
  - Jojoba Oil
  - Hemp Seed Oil
  - Meadow Foam Oil
  - Fractionated Coconut Oil infused with Natural Vanilla Oil
  - Fragrance (Natural Essential Oils)
  - Vitamin E
item_number: 1
price: 15
scent_profile: 820b4505-27ba-4c2e-beb8-51841b82a38a
scent_notes: 7afe2d4e-c891-4d9a-bdda-774b2800cdf2
series: a482e706-3075-40b1-af09-87009ae13603
size: eebd7a27-4f97-45a0-8800-521784215ccb
sku: BO-CSC-TM
weight: 79
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
  - related_product: 944cc97d-91f5-41a9-a094-0767f928f0dc # Tree Mint Mustache Wax
  - related_product: e56efac7-1fcf-48ca-91d1-309d4cec66bc # Original Oil
---
With a blend of deep vanilla, invigorating tea tree, and a hint of peppermint this beard oil is a crowd pleaser. Fresh yet woody, Tree Mint beard oil provides a subtle and intimate scent profile with a lush feel. The new ingredients and updated recipe provide great hold due to its more viscous nature, while still softening and nurturing your beard and skin.