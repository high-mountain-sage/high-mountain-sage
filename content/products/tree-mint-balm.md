---
id: 387bb089-36cc-4596-b36b-e64503423676
category: bd70bd4f-0b5b-42da-bc6d-79d2bdd31d9c
name: Tree Mint Balm
slug: tree-mint-balm
product_image:
  alt_text: >-
    Jars of Tree Mint Balm surrounded by vanilla beans and peppermint
    leaves.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1584637211/tree_mint_balm__single1_s34clw.jpg
ingredients:
  - Shea Butter
  - Beeswax
  - Argan Oil
  - Jojoba Oil
  - Coconut Oil
  - Lanolin
  - Fragrance (Natural Essential Oils)
item_number: 1
price: 20
scent_profile: 820b4505-27ba-4c2e-beb8-51841b82a38a
scent_notes: 7afe2d4e-c891-4d9a-bdda-774b2800cdf2
series: a482e706-3075-40b1-af09-87009ae13603
size: 7acd5143-d0b7-4e74-9775-58bbfa9bfbe6
sku: BM-CSC-TM
weight: 153
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: a3f74318-238d-4604-9a29-3213022e9b52 # Tree Mint Oil
  - related_product: 944cc97d-91f5-41a9-a094-0767f928f0dc # Tree Mint Mustache Wax
  - related_product: 2b346413-2dcb-4ca9-ad3f-a9eeded0a396 # Original Balm
---
One of our top sellers, this handcrafted balm is perfect for your beard, hair and skin (including tattoos). Its scent profile includes a blend of natural vanilla, tea tree and peppermint. This combination results in a subtle, fresh, and forest-like aura. Tree Mint balm provides a medium to light styling hold while still softening and nourishing your beard. This, like all our balms, is handcrafted with natural beeswax from Northern Utah bee colonies and exotic imported ingredients including luxurious shea butter and Moroccan argan oil.