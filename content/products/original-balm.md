---
id: 2b346413-2dcb-4ca9-ad3f-a9eeded0a396
category: bd70bd4f-0b5b-42da-bc6d-79d2bdd31d9c
name: Original Balm
slug: original-balm
product_image:
  alt_text: >-
    Jars of Original Balm surrounded by sliced oranges, dried sage, and shaved
    cedar wood.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1584637211/original_balm__single_xixmrl.jpg
ingredients:
  - Shea Butter
  - Beeswax
  - Argan Oil
  - Jojoba Oil
  - Coconut Oil
  - Lanolin
  - Fragrance (Natural Essential Oils)
  - Vitamin E
item_number: 1
price: 20
scent_profile: 536a1888-17d1-47d4-a3f8-ece88bf01332
scent_notes: 50f9db18-d554-4565-a444-237c23662314
series: a482e706-3075-40b1-af09-87009ae13603
size: 7acd5143-d0b7-4e74-9775-58bbfa9bfbe6
sku: BM-CSC-OG
weight: 153
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: e56efac7-1fcf-48ca-91d1-309d4cec66bc # Original Oil
  - related_product: 6aea8625-f0f9-4bcb-8097-1aba414c3a3e # Original Mustache Wax
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
---
A scent so iconic we named our company after it. A woody, calming, and subtle blend of cedar, sage, and orange. This hybrid balm provides great styling while still softening and nourishing your beard, hair, and skin. We handcrafted this balm, like all our balms, with natural beeswax from Northern Utah bee colonies and exotic imported ingredients including African shea butter and Moroccan argan oil.