---
id: e56efac7-1fcf-48ca-91d1-309d4cec66bc
category: 45fb992b-aac4-4411-821b-e80f3f32decd
name: Original Oil
slug: original-oil
product_image:
  alt_text: >-
    A bottle of Original beard oil surrounded by cut oranges, dried sage, and
    shaved cedar wood. 
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1584637211/original_oil__single_jh35mf.jpg
ingredients:
  - Castor Oil
  - Jojoba Oil
  - Hemp Seed Oil
  - Meadow Foam Oil
  - Fragrance (Natural Essential Oils)
  - Vitamin E
item_number: 1
price: 15
scent_profile: 536a1888-17d1-47d4-a3f8-ece88bf01332
scent_notes: 50f9db18-d554-4565-a444-237c23662314
series: a482e706-3075-40b1-af09-87009ae13603
size: eebd7a27-4f97-45a0-8800-521784215ccb
sku: BO-CSC-OG
weight: 79
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 2b346413-2dcb-4ca9-ad3f-a9eeded0a396 # Original Balm
  - related_product: 6aea8625-f0f9-4bcb-8097-1aba414c3a3e # Original Mustache
  - related_product: a3f74318-238d-4604-9a29-3213022e9b52 # Tree Mint Oil
---
The scent that started it all and for which we named our company after. Notes of cedarwood, sage, and orange create a grounded blend reminiscent of hiking through the high mountains of Utah. The new ingredients and updated recipe craft a thicker beard oil that helps keep “fly-aways” under control while still nourishing, softening, and moisturizing your beard and skin. Truly a subtle yet luxurious feeling beard oil.