---
id: 171c3fe7-67f6-43a4-ae92-22bc818ecf2b
category: 6b0043c8-c37c-4f43-8a4e-b7778dfb7ba2
name: Tree Mint Beer'd Soap
slug: tree-mint-soap
product_image:
  alt_text: >-
    A bar of Original beer'd soap surrounded by vanila beans and peppermint leaves. 
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1586553780/tree_mint_soap__multi1_azqafp.jpg
ingredients:
  - Coconut Oil
  - Beer
  - Castor Oil
  - Olive Oil
  - Lye
  - Shea Butter
  - Avocado Oil
  - Palm Oil
  - Coconut Milk
  - Jojoba Oil
  - Water
  - Essential Oils
  - Salt
  - Spirulina
  - Mica
  - Green Chromium Oxide
item_number: 1
price: 8
scent_profile: 820b4505-27ba-4c2e-beb8-51841b82a38a
scent_notes: 7afe2d4e-c891-4d9a-bdda-774b2800cdf2
series: a482e706-3075-40b1-af09-87009ae13603
size: 519d7723-4e29-4b5c-910b-548b89f7f62e
sku: SP-CSC-TM
weight: 61
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: a3f74318-238d-4604-9a29-3213022e9b52 # Tree Mint Oil
  - related_product: 5acfb11e-c3d5-4175-9dad-7f7a95083acc # Tree Mint Legendary Kit
  - related_product: 811a7345-a8e5-4719-9165-a530c5824f49 # Original Soap
---
Made in collaboration with our friends at [Wild Waters Soapery](https://www.wildwaterssoapery.com){.font-medium .text-teal-700 .hover:text-teal-800}

<br>

This all natural shampoo bar invigorates and gently washes your "beer'd" with every shower. The scent profile is masculine, fresh, and rejuvenating with notes of vanilla, tea tree, and peppermint. Gentle and cleansing for both facial and head hair.

![Beer'd Soap Dimensions: 2"x1.5"x0.75"](../../src/assets/img/soap_dimensions__small.min.svg)

Buy three or more soaps and receive a bulk discount of $4 off your order.