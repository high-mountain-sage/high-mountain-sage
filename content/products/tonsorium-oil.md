---
id: 318ccd7c-883a-465c-8704-522eaf43402bdecd
category: 45fb992b-aac4-4411-821b-e80f3f32decd
name: Tonsorium Oil
slug: tonsorium-oil
product_image:
  alt_text: >-
    A bottle of Tonsorium beard oil against a clean background.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1638465497/tonsorium_oil__single_gllgis.jpg
ingredients:
  - Castor Oil
  - Jojoba Oil
  - Hemp Seed Oil
  - Meadow Foam Oil
  - Fragrance Oils
  - Vitamin E
item_number: 1
price: 15
scent_profile: db5fe952-9b07-4975-a917-d1b36f348777
scent_notes: 5d9ae342-219f-445a-864a-50bebb422085
series: 521c4a50-3254-4b59-bc62-570bb6a5814f
size: eebd7a27-4f97-45a0-8800-521784215ccb
sku: BO-BBR-TS
weight: 79
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: e56efac7-1fcf-48ca-91d1-309d4cec66bc # Original Oil
  - related_product: a3f74318-238d-4604-9a29-3213022e9b52 # Tree Mint Oil
  - related_product: 160fc685-5dc9-49ac-abba-71562a9a7e77 # Smoking Jacket Oil
---
Tonsorium brings a modern spin to a classic barbershop scent. Our second scent in the Barber Series line features a bay rum top note with sweet sandalwood and light oakmoss as base notes.

<br>

We handcraft our Barber Series beard oils to be smooth with a longer lasting aroma. Like all our oils, this series has a thicker consistency and provides the same quality nourishment for your beard and skin that you've come to expect from all High Mountain Sage products.