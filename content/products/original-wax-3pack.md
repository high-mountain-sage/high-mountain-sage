---
id: 999e9894-0e34-400f-a0ed-58dae3059c27
category: 4d5ea960-fc7f-4909-b203-fe69e7a66cac
name: Original Wax - 3 Pack
slug: original-wax-3pack
product_image:
  alt_text: >-
    3 tins of Original Mustache Wax surrounded by sliced oranges, dried sage, and
    shaved cedar wood.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1585672118/original_wax__3pack_jup3cd.jpg
ingredients:
  - Beeswax
  - Lanolin
  - Jojoba Oil
  - Fragrance (Natural Essential Oils)
item_number: 3
price: 12
scent_profile: 536a1888-17d1-47d4-a3f8-ece88bf01332
scent_notes: 50f9db18-d554-4565-a444-237c23662314
series: a482e706-3075-40b1-af09-87009ae13603
size: 11fcb3e7-97df-46b9-a6a7-d008230a3d72
sku: WX-CSC-OG-3PK
weight: 54
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 2b346413-2dcb-4ca9-ad3f-a9eeded0a396 # Original Balm
  - related_product: ba443aed-13ab-4cb5-86f3-37d11773e459 # Original Legendary
  - related_product: f69bc675-efc5-4b26-98b6-414cd4e49eb0 # Tree Mint Wax - 3 pack
---
The scent that started it all and for which our company is named after. Subtle notes of cedarwood, sage, and orange call back to the high mountains of Utah. Our Stache Wax provides a stiffer hold than balms for greater styling capabilities and is handcrafted with natural beeswax from Northern Utah bee colonies. This 3 pack is great for stocking up. Whether you carry a tin on your person or keep a spare in the car or office, your mustache will always look on point.

<br>

For easier application, heat this product up by keeping it in your pocket or blasting it with a blow dryer for a few seconds.