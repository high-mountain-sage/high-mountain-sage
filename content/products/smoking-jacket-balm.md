---
id: 7f2a8465-af6e-4605-b120-7df9b892b872
category: bd70bd4f-0b5b-42da-bc6d-79d2bdd31d9c
name: Smoking Jacket Balm
slug: smoking-jacket-balm
product_image:
  alt_text: >-
    A jar of Smoking Balm against a clean background.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1638465497/smoking_jacket_balm__single_yjtqsb.jpg
ingredients:
  - Shea Butter
  - Beeswax
  - Argan Oil
  - Jojoba Oil
  - Coconut Oil
  - Lanolin
  - Fragrance Oils
  - Vitamin E
item_number: 1
price: 20
scent_profile: db5fe952-9b07-4975-a917-d1b36f348777
scent_notes: 9f5c73b8-2431-479e-92a9-1212c5820b20
series: 521c4a50-3254-4b59-bc62-570bb6a5814f
size: 7acd5143-d0b7-4e74-9775-58bbfa9bfbe6
sku: BM-BBR-SM
weight: 153
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 160fc685-5dc9-49ac-abba-71562a9a7e77 # Smoking Jacket Oil
  - related_product: 2b346413-2dcb-4ca9-ad3f-a9eeded0a396 # Original Balm
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
---
Smoking Jacket has a cologne-like fragrance rich with notes of tobacco, pomegranate, oud, and black velvet. This medium to light balm provides great styling while softening and nourishing your beard, hair, and skin. This, like all our balms, is handcrafted with natural beeswax from Northern Utah colonies and exotic imported ingredients like luxurious shea butter and Moroccan argan oil.

<br>

Our Barber Series balms are handcrafted to be smooth while providing a longer lasting aroma than our other scent lines.