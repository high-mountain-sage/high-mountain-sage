---
id: f69bc675-efc5-4b26-98b6-414cd4e49eb0
category: 4d5ea960-fc7f-4909-b203-fe69e7a66cac
name: Tree Mint Wax - 3 Pack
slug: tree-mint-wax-3pack
product_image:
  alt_text: >-
    3 tins of Tree Mint Mustache Wax surrounded by vanilla beans and peppermint
    leaves.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1585672118/tree_mint_wax__3pack_yu6ofw.jpg
ingredients:
  - Beeswax
  - Lanolin
  - Jojoba Oil
  - Fragrance (Natural Essential Oils)
item_number: 3
price: 12
scent_profile: 820b4505-27ba-4c2e-beb8-51841b82a38a
scent_notes: 7afe2d4e-c891-4d9a-bdda-774b2800cdf2
series: a482e706-3075-40b1-af09-87009ae13603
size: 11fcb3e7-97df-46b9-a6a7-d008230a3d72
sku: WX-CSC-TM-3PK
weight: 54
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
  - related_product: 5acfb11e-c3d5-4175-9dad-7f7a95083acc # Tree Mint Legendary
  - related_product: 999e9894-0e34-400f-a0ed-58dae3059c27 # Original Wax - 3 pack
---
An aroma reminiscent of a cozy cabin in a mountainous forest. Tree Mint is subtle and fresh with notes of deep vanilla, tea tree, and peppermint. This mustache wax is pleasing with the right strength for being under your nose all day. Made with local filtered beeswax from the beehive state. This 3 pack is great for stocking up. Whether you carry a tin on your person or keep a spare in the car or office, your mustache will always look on point.

<br>

Stache Wax provides a stiffer hold than balms for greater styling capabilities and is perfect for “stashing” in your pocket when on the go. For easier application, heat this product up by keeping it in your pocket or blasting it with a blow dryer for a few seconds.
