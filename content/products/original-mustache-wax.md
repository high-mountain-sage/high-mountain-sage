---
id: 6aea8625-f0f9-4bcb-8097-1aba414c3a3e
category: 4d5ea960-fc7f-4909-b203-fe69e7a66cac
name: Original Mustache Wax
slug: original-mustache-wax
product_image:
  alt_text: >-
    Tins of Original Mustache Wax surrounded by sliced oranges, dried sage, and
    shaved cedar wood.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1584637211/original_wax__single_pzvefq.jpg
ingredients:
  - Beeswax
  - Lanolin
  - Jojoba Oil
  - Fragrance (Natural Essential Oils)
item_number: 1
price: 5
scent_profile: 536a1888-17d1-47d4-a3f8-ece88bf01332
scent_notes: 50f9db18-d554-4565-a444-237c23662314
series: a482e706-3075-40b1-af09-87009ae13603
size: 32be68d5-1303-4f9b-8baf-fd7c47f415ea
sku: WX-CSC-OG
weight: 18
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 999e9894-0e34-400f-a0ed-58dae3059c27 # Original Wax - 3 pack
  - related_product: 2b346413-2dcb-4ca9-ad3f-a9eeded0a396 # Original Balm
  - related_product: 944cc97d-91f5-41a9-a094-0767f928f0dc # Tree Mint Mustache Wax
---
Purchase individually or save with a [pack of three](/mustache-waxes/original-wax-3pack/){.font-medium .text-teal-700 .hover:text-teal-800}.

<br>

The scent that started it all and for which our company is named after. Subtle notes of cedarwood, sage, and orange call back to the high mountains of Utah. Our Stache Wax provides a stiffer hold than balms for greater styling capabilities and is handcrafted with natural beeswax from Northern Utah bee colonies. Perfect for “stashing” in your pocket when on the go.

<br>

For easier application, heat this product up by keeping it in your pocket or blasting it with a blow dryer for a few seconds.