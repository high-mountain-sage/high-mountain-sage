---
id: 944cc97d-91f5-41a9-a094-0767f928f0dc
category: 4d5ea960-fc7f-4909-b203-fe69e7a66cac
name: Tree Mint Mustache Wax
slug: tree-mint-mustache-wax
product_image:
  alt_text: >-
    Tins of Tree Mint Mustache Wax surrounded by vanilla beans and peppermint
    leaves.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1584637211/tree_mint_wax__single_sdvffj.jpg
ingredients:
  - Beeswax
  - Lanolin
  - Jojoba Oil
  - Fragrance (Natural Essential Oils)
item_number: 1
price: 5
scent_profile: 820b4505-27ba-4c2e-beb8-51841b82a38a
scent_notes: 7afe2d4e-c891-4d9a-bdda-774b2800cdf2
series: a482e706-3075-40b1-af09-87009ae13603
size: 32be68d5-1303-4f9b-8baf-fd7c47f415ea
sku: WX-CSC-TM
weight: 18
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: f69bc675-efc5-4b26-98b6-414cd4e49eb0 # Tree Mint Wax - 3 pack
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
  - related_product: 6aea8625-f0f9-4bcb-8097-1aba414c3a3e # Original Mustache Wax
---
Purchase individually or save with a [pack of three](/mustache-waxes/tree-mint-wax-3pack/){.font-medium .text-teal-700 .hover:text-teal-800}.

<br>

An aroma reminiscent of a cozy cabin in a mountainous forest. Tree Mint is subtle and fresh with notes of deep vanilla, tea tree, and peppermint. This mustache wax is pleasing with the right potency for being under your nose all day. Made with local filtered beeswax from the beehive state.

<br/>

Stache Wax provides a stiffer hold than balms for greater styling capabilities and is perfect for “stashing” in your pocket when on the go. For easier application, heat this product up by keeping it in your pocket or blasting it with a blow dryer for a few seconds.
