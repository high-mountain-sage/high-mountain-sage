---
id: ba443aed-13ab-4cb5-86f3-37d11773e459
category: d9374ec2-4198-4b7c-ac8b-c2bf24e2464b
name: Original Legendary Kit
slug: original-legendary-kit
product_image:
  alt_text: >-
    A bottle, jar, metal tin, bar of Original beard oil, balm, mustache wax, and soap surrounded by cut oranges, dried sage, and
    shaved cedar wood. 
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1586553780/original_legend_gtkqrl.jpg
ingredients:
  - Original Balm
  - Original Beer'd Soap
  - Original Mustache Wax
  - Original Oil
item_number: 4
price: 40
scent_profile: 536a1888-17d1-47d4-a3f8-ece88bf01332
scent_notes: 50f9db18-d554-4565-a444-237c23662314
series: a482e706-3075-40b1-af09-87009ae13603
size: 27f68a16-47d8-45af-81bc-0bc6f6907ef4
sku: KT-CSC-OG-LGD
weight: 311
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 811a7345-a8e5-4719-9165-a530c5824f49 # Original Soap
  - related_product: 2b346413-2dcb-4ca9-ad3f-a9eeded0a396 # Original Balm
  - related_product: 5acfb11e-c3d5-4175-9dad-7f7a95083acc # Tree Mint Legendary Kit
---
You are known by all across the land. Tales of your epic style, charm, and facial hair precede you where'er you go. That's why you are legendary. Beer'd soap, beard oil, mustache wax, and balm all together in one kit so you can cleanse, soften, and shape your illustrious beard and distinguished mustache; giving you everything you need to keep spreading your fame far and wide.

<br>

Our Original scent transports you to the mountains with its rugged woody scent of cedar, sage, and a top note of orange.