---
id: 5acfb11e-c3d5-4175-9dad-7f7a95083acc
category: d9374ec2-4198-4b7c-ac8b-c2bf24e2464b
name: Tree Mint Legendary Kit
slug: tree-mint-legendary-kit
product_image:
  alt_text: >-
    A bottle, jar, metal tin, bar of Tree Mint beard oil, balm, mustache wax, and soap surrounded by vanila beans and peppermint leaves. 
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1586553780/tree_mint_legend_ryfake.jpg
ingredients:
  - Tree Mint Balm
  - Tree Mint Beer'd Soap
  - Tree Mint Mustache Wax
  - Tree Mint Oil
item_number: 4
price: 40
scent_profile: 820b4505-27ba-4c2e-beb8-51841b82a38a
scent_notes: 7afe2d4e-c891-4d9a-bdda-774b2800cdf2
series: a482e706-3075-40b1-af09-87009ae13603
size: 27f68a16-47d8-45af-81bc-0bc6f6907ef4
sku: KT-CSC-TM-LGD
weight: 311
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: 171c3fe7-67f6-43a4-ae92-22bc818ecf2b # Tree Mint Soap
  - related_product: 387bb089-36cc-4596-b36b-e64503423676 # Tree Mint Balm
  - related_product: ba443aed-13ab-4cb5-86f3-37d11773e459 # Original Legendary Kit
---
You are known by all across the land. Tales of your epic style, charm, and facial hair precede you where'er you go. That's why you are legendary. Beer'd soap, beard oil, mustache wax, and balm all together in one kit so you can cleanse, soften, and shape your illustrious beard and distinguished mustache; giving you everything you need to keep spreading your fame far and wide.

<br>

Tree Mint gives you that fresh, just showered after an adventure blend of vanilla, tea tree, and peppermint.