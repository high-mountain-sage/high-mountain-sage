---
id: 160fc685-5dc9-49ac-abba-71562a9a7e77
category: 45fb992b-aac4-4411-821b-e80f3f32decd
name: Smoking Jacket Oil
slug: smoking-jacket-oil
product_image:
  alt_text: >-
    A bottle of Smoking Jacket beard oil surrounded by tobacco, pomegranates, and black velvet.
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1596749289/smoking_jacket__multi_rcddgm.jpg
ingredients:
  - Castor Oil
  - Jojoba Oil
  - Hemp Seed Oil
  - Meadow Foam Oil
  - Fragrance Oils
  - Vitamin E
item_number: 1
price: 15
scent_profile: db5fe952-9b07-4975-a917-d1b36f348777
scent_notes: 9f5c73b8-2431-479e-92a9-1212c5820b20
series: 521c4a50-3254-4b59-bc62-570bb6a5814f
size: eebd7a27-4f97-45a0-8800-521784215ccb
sku: BO-BBR-SM
weight: 79
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: e56efac7-1fcf-48ca-91d1-309d4cec66bc # Original Oil
  - related_product: a3f74318-238d-4604-9a29-3213022e9b52 # Tree Mint Oil
  - related_product: 7f2a8465-af6e-4605-b120-7df9b892b872 # Smoking Jacket Balm
---
Sexy, intimate, and earthy is the best way to describe this first scent created for our Barber Series line of products. Smoking Jacket's cologne-like fragrance includes rich notes of tobacco, pomegranate, oud, and black velvet. This is rustic high-end luxury, period.

<br>

Our Barber Series beard oils are handcrafted to be smooth while providing a longer lasting aroma than our other scent lines. Like all our oils, this series has a thicker viscosity and provides the same quality nourishment for your beard and skin that you've come to expect from all High Mountain Sage products.
