---
id: 811a7345-a8e5-4719-9165-a530c5824f49
category: 6b0043c8-c37c-4f43-8a4e-b7778dfb7ba2
name: Original Beer'd Soap
slug: original-soap
product_image:
  alt_text: >-
    A bar of Original beer'd soap surrounded by cut oranges, dried sage, and
    shaved cedar wood. 
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1586553780/original_soap__multi1_mklmzf.jpg
ingredients:
  - Coconut Oil
  - Beer
  - Castor Oil
  - Olive Oil
  - Lye
  - Shea Butter
  - Avocado Oil
  - Palm Oil
  - Coconut Milk
  - Jojoba Oil
  - Water
  - Essential Oils
  - Salt
  - Cocoa Powder
  - Spirulina
item_number: 1
price: 8
scent_profile: 536a1888-17d1-47d4-a3f8-ece88bf01332
scent_notes: 50f9db18-d554-4565-a444-237c23662314
series: a482e706-3075-40b1-af09-87009ae13603
size: 519d7723-4e29-4b5c-910b-548b89f7f62e
sku: SP-CSC-OG
weight: 61
status: 5484176a-e7b9-4b08-a293-ddd5860a6d78
public: true
related_products:
  - related_product: e56efac7-1fcf-48ca-91d1-309d4cec66bc # Original Oil
  - related_product: ba443aed-13ab-4cb5-86f3-37d11773e459 # Original Legendary Kit
  - related_product: 171c3fe7-67f6-43a4-ae92-22bc818ecf2b # Tree Mint Soap
---
Made in collaboration with our friends at [Wild Waters Soapery](https://www.wildwaterssoapery.com){.font-medium .text-teal-700 .hover:text-teal-800}

<br>

The scent that started it all and for which our company is named after. An all natural shampoo bar crafted to cleanse your "beer'd" all while smelling like the mountainous woods of Northern Utah. Original Beer'd Soap welcomes you into the shower with warm, subtle notes of cedarwood, sage, and orange. Gentle and cleansing for both facial and head hair.

![Beer'd Soap Dimensions: 2"x1.5"x0.75"](../../src/assets/img/soap_dimensions__small.min.svg)

Buy three or more soaps and receive a bulk discount of $4 off your order.