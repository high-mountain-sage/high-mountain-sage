---
id: f2bb2176-77f5-476f-bd8e-b24676985e2d
category: 45fb992b-aac4-4411-821b-e80f3f32decd
name: Barrel Fever
slug: barrel-fever
product_image:
  alt_text: >-
    Bottles of Barrel Fever surrounded by a bottle of bourbon, playing cards,
    and billiard accessories. 
  photo: >-
    https://res.cloudinary.com/high-mountain-sage/image/upload/v1579197731/barrel_fever_oil_rmxfxp.jpg
ingredients:
  - Jojoba Oil
  - Argan Oil
  - Castor Oil
  - Grapeseed Oil aged with Kentucky Bourbon
  - Fragrance (Natural Essential Oil)
  - Vitamin E
item_number: 1
price: 20
scent_profile: 48e60baa-92c5-44a9-b12c-b59ec472d38d
series: afdcc7a4-cf3e-42ab-9f98-25ffaefb635b
size: 7acd5143-d0b7-4e74-9775-58bbfa9bfbe6
weight: 1
status: b9a9682c-5480-49dc-82cc-4467a924748f
public: false
---
To craft our premium Barrel Fever oil, we mix real Kentucky bourbon with grapeseed oil for a minimum of one month. We separate the oil from the alcohol through a special process keeping that smoky bourbon scent. Blended with sandalwood essential oil we create a sweet, but woody scent.

* May contain trace amounts of alcohol, not for internal consumption.
