---
id: 22eaa9f9-a758-4e56-a0bf-4d0ba15ab89b
name: Sweet Mint
notes:
  - Vanilla
  - Tea Tree
  - Peppermint
  - Clove
---