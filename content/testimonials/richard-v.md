---
id: 84150f20-3ee6-4497-8588-c2c6bea3ed1e
name: Richard V.
location: Park City, UT
photo: https://res.cloudinary.com/high-mountain-sage/image/upload/v1616621244/richard_v_awmfqq.jpg
featured: true
---
"I find High Mountain Sage to stand above other products I have tried."
