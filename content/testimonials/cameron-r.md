---
id: 54969280-513f-494b-88a2-981ba779ab74
name: Cameron R.
location: Logan, UT
photo: https://res.cloudinary.com/high-mountain-sage/image/upload/v1616619846/cam_r_z25gkf.jpg
featured: false
---
"Incredible hold paired with impeccable smell.
What more can you ask for to keep those wispy stache hairs in place?
This stuff will. Keeps my stache looking great all day!"