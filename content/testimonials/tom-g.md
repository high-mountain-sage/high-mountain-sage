---
id: e9640609-6a76-4c8b-a69c-5ea4558ddd81
name: Tom G.
location: Shalimar, FL
photo: https://res.cloudinary.com/high-mountain-sage/image/upload/v1616621671/tom_g_und8sx.jpg
featured: true
---
"I have tried many products, but your balm and wax are still among the best performing, and I've always liked the smell of both varieties."