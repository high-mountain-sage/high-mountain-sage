---
id: 39200de3-1ff3-46c2-88a2-7b7e5345996a
name: Nick J.
location: Murray, UT
photo: https://res.cloudinary.com/high-mountain-sage/image/upload/v1616621244/nick_j_wsdbvu.jpg
featured: true
---
"A wax that can keep my moustache under control is a rare find. It also smells nice."