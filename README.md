# Project Overview

This was the JAMStack e-commerce site for High Mountain Sage 🧔🏻‍♂️ from 02/2020 to 02/2022.
It was built with [Gridsome](https://gridsome.org)(Vue), integrated with
[Snipcart](https://snipcart.com) and [Easypost](https://easypost.com) and deployed on Netlify.

## Screenshots
![Home page of High Mountain Sage](/screenshots/high_mountain_sage-home_page.png)
![Products overview page of High Mountain Sage](/screenshots/high_mountain_sage-products_overview.png)
![Product detail page for High Mountain Sage](/screenshots/high_mountain_sage-product_page.png)
![Mobile layout of product detail page](/screenshots/high_mountain_sage-product_page-mobile.png)

## Why won't the site compile?

The original site used a font licensed through Linotype. As a part of the license,
the font needed to be hosted in a manner that it couldn't be downloaded and use
by anyone other than the licensee. As the repo was originally private, this
statisfied the requirement. Now that the repo is public, the font has been removed
and the git history references have been scrubbed with [BFG](https://rtyley.github.io/bfg-repo-cleaner/).

If I find an open source font that resembles the missing font, I'll update the
project. For now, to get a visual sense of the site please refer to the screenshots.

## There appear to be keys in the project's repo?

Any remaining keys in the repo are public keys that could be easily accessed by
viewing the page source through a browser. I debated about removing them with
BFG (like the paid font), however decided to keep them in to better illustrate
how I setup the project to use test keys for development and use
the live keys for production.